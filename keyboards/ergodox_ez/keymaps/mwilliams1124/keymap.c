#include QMK_KEYBOARD_H
#include "version.h"
#include "keymap_german.h"
#include "keymap_nordic.h"
#include "keymap_french.h"
#include "keymap_spanish.h"
#include "keymap_hungarian.h"
#include "keymap_swedish.h"
#include "keymap_br_abnt2.h"
#include "keymap_canadian_multilingual.h"
#include "keymap_german_ch.h"

#define KC_MAC_UNDO LGUI(KC_Z)
#define KC_MAC_CUT LGUI(KC_X)
#define KC_MAC_COPY LGUI(KC_C)
#define KC_MAC_PASTE LGUI(KC_V)
#define KC_PC_UNDO LCTL(KC_Z)
#define KC_PC_CUT LCTL(KC_X)
#define KC_PC_COPY LCTL(KC_C)
#define KC_PC_PASTE LCTL(KC_V)
#define NO_TH ALGR(KC_T)
#define NO_ETH ALGR(KC_D)

#define PRIMARY 0 // Default layer
#define NUMPAD 1 // WIP
#define VIM_KEYS 2 // WIP
#define FUNCTION 3 // WIP

enum custom_keycodes {
  RGB_SLD = SAFE_RANGE, // can always be here
  TOGGLE_LAYER_COLOR,
  EPRM,
  DYNAMIC_MACRO_RANGE,
  QMKBEST,
  AUTO_PAREN,
  AUTO_BRACKET,
  AUTO_QUOTE,
};
#include "dynamic_macro.h"

// Considerations:
// Shift, Ctrl & Alt output their normal value and at same time switch to function layer.
// Need another layer key for function layer without chording
// F11-20?
// LM(layer, mod) - Momentarily activates layer (like MO), but with modifier(s) mod active. Only supports layers 0-15 and the left modifiers: MOD_LCTL, MOD_LSFT, MOD_LALT, MOD_LGUI (note the use of MOD_ constants instead of KC_). These modifiers can be combined using bitwise OR, e.g. LM(FUNC, MOD_LCTL | MOD_LALT).
//
// Vim movement layer?
// Numpad layer
//
// Pad layer - shebangs, boilerplate etc
//
// Use Tap Dance for RESET?

// ::::: KEYMAPS {{{1

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

[PRIMARY] = LAYOUT_ergodox( //:{{{2

    /* keymap-diagram:
     * ,--------------------------------------------------.    ,--------------------------------------------------.
     * |    ~   |   1  |   2  |   3  |   4  |   5  |  [   |    |  ]   |   6  |   7  |   8  | 9/-  | 0/=  |   ''   |
     * |--------+------+------+------+------+------+------|    |------+------+------+------+------+------+--------|
     * |  Tab   |   Q  |   W  |   E  |   R  |   T  |  ()  |    |  []  |   Y  |   U  |   I  |   O  |   P  |   \    |
     * |--------+------+------+------+------+------|      |    |      |------+------+------+------+------+--------|
     * |  Ctrl  |   A  |   S  |   D  |   F  |   G  |------|    |------|   H  |   J  |   K  |   L  |   ;  |   '    |
     * |--------+------+------+------+------+------|  (   |    |  )   |------+------+------+------+------+--------|
     * |  Shft  |   Z  |   X  |   C  |   V  |   B  |      |    |      |   N  |   M  |   ,  |   .  |   /  |  Shft  |
     * `--------+------+------+------+------+-------------'    `-------------+------+------+------+------+--------'
     *   | PgUp | PgDn | Home | End  | Bksp |                                | Del  | Ins  | Meh  | SysR | Hypr |
     *   `----------------------------------'                                `----------------------------------'
     *                                       ,-------------.  ,-------------.
     *                                       | Meta |  Fx  |  |  Fx  | Meta |
     *                                ,------+------+------|  |------+------+------.
     *                                |      |      | NmPd |  |  -   |      |      |
     *                                | Entr | VimM |------|  |------| Ctrl | Spc  |
     *                                |      |      | Alt  |  | Alt  |      |      |
     *                                `--------------------'  `--------------------'
     */
//Caps KC_CAPSLOCK - make this available somewhere. Shift double tap??
//Remove one of the Fx layer keys once you have auto-switch-to-fx-layer-with-modifier working

// Left Hand {{{3

KC_GRAVE , KC_1     , KC_2   , KC_3  , KC_4     , KC_5   , KC_LBRC   ,
KC_TAB   , KC_Q     , KC_W   , KC_E  , KC_R     , KC_T   , AUTO_PAREN,
KC_LCTRL , KC_A     , KC_S   , KC_D  , KC_F     , KC_G   ,
KC_LSHIFT, KC_Z     , KC_X   , KC_C  , KC_V     , KC_Q   , KC_LPRN   ,
KC_PGUP  , KC_PGDOWN, KC_HOME, KC_END, KC_BSPACE,						// 3}}}

// Left Thumb {{{3

          KC_LGUI      , MO(3)  ,
                         TT(1)  ,
KC_ENTER, LT(2,_______), KC_LALT,									// 3}}}

// Right Hand {{{3

//KC_RBRC          , KC_6, KC_7  , KC_8, KC_9, KC_0 , DYN_REC_STOP,
KC_RBRC     , KC_6, KC_7     , KC_8     , KC_P9 , KC_P0     , AUTO_QUOTE,
AUTO_BRACKET, KC_Y, KC_U     , KC_I     , KC_O  , KC_P      , KC_BSLASH ,
              KC_H, KC_J     , KC_K     , KC_L  , KC_SCOLON , KC_QUOTE  ,
KC_RPRN     , KC_N, KC_M     , KC_COMMA , KC_DOT, KC_SLASH  , KC_RSHIFT ,
                    KC_DELETE, KC_INSERT, KC_MEH, KC_PSCREEN, KC_HYPR   ,			// 3}}}

// Right Thumb {{{3

MO(3)  , KC_RGUI ,
_______,
KC_RALT, KC_RCTRL, KC_SPACE									//3}}}

), // 2}}}

[NUMPAD] = LAYOUT_ergodox( //:{{{2

    /* keymap-diagram:
     * ,--------------------------------------------------.    ,--------------------------------------------------.
     * |    -   |   -  |   -  |   -  |   -  |   -  |   -  |    |   -  |   -  |   -  |   -  |   -  |   -  |    -   |
     * |--------+------+------+------+------+------+------|    |------+------+------+------+------+------+--------|
     * |    -   |   -  |   -  |   -  |   -  |   -  |   -  |    |   -  |   -  |   -  |   -  |   -  |   -  |    -   |
     * |--------+------+------+------+------+------|      |    |      |------+------+------+------+------+--------|
     * |    -   |   -  |   -  |   -  |   -  |   -  |------|    |------|   -  |   -  |   -  |   -  |   -  |    -   |
     * |--------+------+------+------+------+------|   -  |    |   -  |------+------+------+------+------+--------|
     * |    -   |   -  |   -  |   -  |   -  |   -  |      |    |      |   -  |   -  |   -  |   -  |   -  |    -   |
     * `--------+------+------+------+------+-------------'    `-------------+------+------+------+------+--------'
     *   |  -   |   -  |   -  |   -  |   -  |                                |   -  |   -  |   -  |   -  |   -  |
     *   `----------------------------------'                                `----------------------------------'
     *                                       ,-------------.  ,-------------.
     *                                       |   -  |   -  |  |   -  |   -  |
     *                                ,------+------+------|  |------+------+------.
     *                                |      |      |   -  |  |   -  |      |      |
     *                                |   -  |   -  |------|  |------|   -  |   -  |
     *                                |      |      |   -  |  |   -  |      |      |
     *                                `--------------------'  `--------------------'
     */

// Left Hand {{{3

_______  , _______, _______, _______, _______, _______, _______          ,
_______  , _______, _______, _______, _______, _______, KC_AUDIO_VOL_DOWN,
KC_ESCAPE, _______, _______, _______, _______, _______,
_______  , _______, _______, _______, _______, _______, _______          ,
_______  , _______, _______, _______, _______,							//3}}}

// Left Thumb {{{3

         _______, _______,
                  _______,
_______, _______, _______,									//3}}}

// Right Hand {{{3


KC_AUDIO_MUTE  , _______, KC_EQUAL, KC_KP_SLASH, KC_KP_ASTERISK, KC_KP_MINUS, _______,
KC_AUDIO_VOL_UP, _______, KC_7    , KC_8       , KC_9          , KC_KP_PLUS , _______,
                 _______, KC_4    , KC_5       , KC_6          , KC_PLUS    , _______,
_______        , _______, KC_1    , KC_2       , KC_3          , KC_KP_ENTER, _______,
                          KC_KP_0 , KC_KP_0    , KC_KP_DOT     , KC_KP_ENTER, _______,		//3}}}

// Right Thumb {{{3

KC_MEDIA_REWIND    , KC_MEDIA_FAST_FORWARD,
KC_MEDIA_PREV_TRACK,
KC_MEDIA_NEXT_TRACK, _______              , _______						//3}}}

), //2}}}

[VIM_KEYS] = LAYOUT_ergodox( //:{{{2

    /* keymap-diagram:
     * ,--------------------------------------------------.    ,--------------------------------------------------.
     * |    -   |   -  |   -  |   -  |   -  |   -  |   -  |    |   -  |   -  |   -  |   -  |   -  |   -  |    -   |
     * |--------+------+------+------+------+------+------|    |------+------+------+------+------+------+--------|
     * |    -   |   -  |   -  |   -  |   -  |   -  |   -  |    |   -  |   -  |   -  |   -  |   -  |   -  |    -   |
     * |--------+------+------+------+------+------|      |    |      |------+------+------+------+------+--------|
     * |    -   |   -  |   -  |   -  |   -  |   -  |------|    |------|   -  |   -  |   -  |   -  |   -  |    -   |
     * |--------+------+------+------+------+------|   -  |    |   -  |------+------+------+------+------+--------|
     * |    -   |   -  |   -  |   -  |   -  |   -  |      |    |      |   -  |   -  |   -  |   -  |   -  |    -   |
     * `--------+------+------+------+------+-------------'    `-------------+------+------+------+------+--------'
     *   |  -   |   -  |   -  |   -  |   -  |                                |   -  |   -  |   -  |   -  |   -  |
     *   `----------------------------------'                                `----------------------------------'
     *                                       ,-------------.  ,-------------.
     *                                       |   -  |   -  |  |   -  |   -  |
     *                                ,------+------+------|  |------+------+------.
     *                                |      |      |   -  |  |   -  |      |      |
     *                                |   -  |   -  |------|  |------|   -  |   -  |
     *                                |      |      |   -  |  |   -  |      |      |
     *                                `--------------------'  `--------------------'
     */

// Left Hand {{{3

_______, KC_F1  , KC_F2     , KC_F3     , KC_F4      , KC_F5  , KC_F6  ,
_______, _______, _______   , KC_MS_UP  , _______    , _______, _______,
_______, _______, KC_MS_LEFT, KC_MS_DOWN, KC_MS_RIGHT, _______,
_______, _______, KC_MS_BTN2, KC_MS_BTN3, KC_MS_BTN1 , _______, _______,
_______, _______, _______   , _______   , _______    ,						//3}}}

// Left Thumb {{{3

         KC_MS_WH_UP, KC_MS_WH_DOWN,
                      _______      ,
_______, _______    , _______      ,								//3}}}

// Right Hand {{{3

KC_F7  , KC_F8  , KC_F9  , KC_F10 , KC_F11  , KC_F12 , _______,
_______, _______, _______, _______, _______ , _______, _______,
         KC_LEFT, KC_DOWN, KC_UP  , KC_RIGHT, _______, _______,
_______, _______, _______, _______, _______ , _______, _______,
                  _______, _______, _______ , _______, _______,					//3}}}

// Right Thumb {{{3

KC_MS_WH_LEFT, KC_MS_WH_RIGHT,
_______      ,
_______      , _______       , _______								//3}}}

), //2}}}

[FUNCTION] = LAYOUT_ergodox( //:{{{2

    /* keymap-diagram:
     * ,--------------------------------------------------.    ,--------------------------------------------------.
     * |    -   |  F1  |  F2  |  F3  |  F4  |  F5  |   -  |    |   -  |  F6  |  F7  |  F8  |  F9  | F10  |    -   |
     * |--------+------+------+------+------+------+------|    |------+------+------+------+------+------+--------|
     * |    -   |   -  |   -  |   -  |   -  |   -  |      |    |      |   -  |   -  |   -  |   -  |   -  |    -   |
     * |--------+------+------+------+------+------|      |    |      |------+------+------+------+------+--------|
     * |    -   |   -  |   -  |   -  |   -  |   -  |------|    |------|   -  |   -  |   -  |   -  |   -  |    -   |
     * |--------+------+------+------+------+------|   -  |    |   -  |------+------+------+------+------+--------|
     * |    -   |   -  |   -  |   -  |   -  |   -  |      |    |      |   -  |   -  |   -  |   -  |   -  |    -   |
     * `--------+------+------+------+------+-------------'    `-------------+------+------+------+------+--------'
     *   | F11  | F12  | F13  | F14  | F15  |                                | F16  | F17  | F18  | F19  | F20  |
     *   `----------------------------------'                                `----------------------------------'
     *                                       ,-------------.  ,-------------.
     *                                       |   -  |   -  |  |   -  |   -  |
     *                                ,------+------+------|  |------+------+------.
     *                                |      |      |   -  |  |   -  |      |      |
     *                                |   -  |   -  |------|  |------|   -  |   -  |
     *                                |      |      |   -  |  |   -  |      |      |
     *                                `--------------------'  `--------------------'
     */

// Left Hand {{{3

_______, KC_F1  , KC_F2  , KC_F3  , KC_F4  , KC_F5  , _______        ,
_______, _______, _______, _______, _______, _______, _______        ,
_______, _______, _______, _______, _______, _______,
_______, _______, _______, _______, _______, _______, DYN_MACRO_PLAY1,
KC_F10 , KC_F12 , KC_F13 , KC_F14 , KC_F15 ,							//3}}}

// Left Thumb {{{3

         DYN_REC_START1, DYN_REC_START2,
                         _______       ,
_______, _______       , _______       ,							//3}}}

// Right Hand {{{3

_______        , KC_F6  , KC_F7  , KC_F8  , KC_F9  , KC_F10 , _______,
_______        , _______, _______, _______, _______, _______, _______,
                 _______, _______, _______, _______, _______, _______,
DYN_MACRO_PLAY2, _______, _______, _______, _______, _______, _______,
                          KC_F16 , KC_F17 , KC_F18 , KC_F19 , KC_F20 ,				//3}}}

// Right Thumb {{{3

_______, _______,
_______,
_______, _______, _______									//3}}}

), //2}}}

}; //1}}}

// ::::: Key Combos {{{

// https://beta.docs.qmk.fm/features/feature_combo

enum combos {
  CTRL_ESC,
  ALT_MINUS,
  ALT_EQUAL
};

const uint16_t PROGMEM esc_combo[] = {KC_LCTRL, KC_RCTRL, COMBO_END};
const uint16_t PROGMEM minus_combo[] = {KC_LCTRL, KC_P9, COMBO_END};
const uint16_t PROGMEM equal_combo[] = {KC_LCTRL, KC_P0, COMBO_END};

combo_t key_combos[COMBO_COUNT] = {
  [CTRL_ESC] = COMBO(esc_combo, KC_ESC),
  [ALT_MINUS] = COMBO(minus_combo, KC_MINUS),
  [ALT_EQUAL] = COMBO(equal_combo, KC_EQUAL)
}; //}}}

bool suspended = false;

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  if (!process_record_dynamic_macro(keycode, record)) {
    return false;
  }
  switch (keycode) {
    case EPRM:
      if (record->event.pressed) {
        eeconfig_init();
      }
      return false;
      break;
    case RGB_SLD:
      if (record->event.pressed) {
        rgblight_mode(1);
      }
      return false;
      break;
    case QMKBEST:
      if (record->event.pressed) {
        SEND_STRING ("QMK is the blurst!");
      }
      return false;
      break;
    case AUTO_PAREN:
      if (record->event.pressed) {
        SEND_STRING ("()");
        clear_mods();
        SEND_STRING (SS_TAP(X_LEFT));
      }
      return false;
      break;
    case AUTO_BRACKET:
      if (record->event.pressed) {
        SEND_STRING ("[]");
        clear_mods();
        SEND_STRING (SS_TAP(X_LEFT));
      } 
      return false;
      break;
    case AUTO_QUOTE:
      if (record->event.pressed) {
        SEND_STRING ("''");
        clear_mods();
        SEND_STRING (SS_TAP(X_LEFT));
      }
      return false;
      break;
  }
  return true;
}

uint32_t layer_state_set_user(uint32_t state) {

    uint8_t layer = biton32(state);

    ergodox_board_led_off();
    ergodox_right_led_1_off();
    ergodox_right_led_2_off();
    ergodox_right_led_3_off();
    switch (layer) {
      case 1:
        ergodox_right_led_1_on();
        break;
      case 2:
        ergodox_right_led_2_on();
        break;
      case 3:
        ergodox_right_led_3_on();
        break;
      case 4:
        ergodox_right_led_1_on();
        ergodox_right_led_2_on();
        break;
      case 5:
        ergodox_right_led_1_on();
        ergodox_right_led_3_on();
        break;
      case 6:
        ergodox_right_led_2_on();
        ergodox_right_led_3_on();
        break;
      case 7:
        ergodox_right_led_1_on();
        ergodox_right_led_2_on();
        ergodox_right_led_3_on();
        break;
      default:
        break;
    }
    return state;

};
